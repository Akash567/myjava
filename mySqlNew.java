package webplayer;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class PrimaryData{
	private String name;
	private String mobilenumber;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobilenumber() {
		return mobilenumber;
	}
	public void setMobilenumber(String mobilenumber) {
		this.mobilenumber = mobilenumber;
	}
}
class SecondryData{
	private String mobilenumber;
	private String address;
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMobilenumber() {
		return mobilenumber;
	}
	public void setMobilenumber(String mobilenumber) {
		this.mobilenumber = mobilenumber;
	}
	
}
class DataBaseResponse{
	public List<PrimaryData> getPrimaryData() throws SQLException, ClassNotFoundException{
		List<PrimaryData> primaryData = new ArrayList<PrimaryData>();
		
		Class.forName("com.mysql.jdbc.Driver");
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tabless","root","5697");
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("select*from primaryData");
		while(rs.next()) {
			PrimaryData primdata = new PrimaryData();
			primdata.setName(rs.getString("name"));
			primdata.setMobilenumber(rs.getString("mobilenumber"));
			
			primaryData.add(primdata);
	}
		
		return primaryData;
	}
		public List<SecondryData> getSecondryData() throws SQLException, ClassNotFoundException{
			List<SecondryData> secondData = new ArrayList<SecondryData>();
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tabless","root","5697");
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("select*from SecondryData");
			while(rs.next()) {
				SecondryData seconddata = new SecondryData();
				seconddata.setMobilenumber(rs.getString("mobilenumber"));
				seconddata.setAddress(rs.getString("address"));
				
				secondData.add(seconddata);
			
			}
			return secondData;
}

	public class mySqlNew {
	public static void main(String []args) throws SQLException, ClassNotFoundException {
		DataBaseResponse dbs = new DataBaseResponse();
		List<PrimaryData> pp = dbs.getPrimaryData();
		List<SecondryData> sd = dbs.getSecondryData();
		
		Map<String, PrimaryData> prim = new HashMap<>();
		for(PrimaryData ppp : pp) {
			String name = ppp.getName();
			String monum = ppp.getMobilenumber();
			prim.put(monum, ppp);
		}
		Map<String, SecondryData> second = new HashMap<>();
		for(SecondryData secc : sd) {
			String monum = secc.getMobilenumber();
			String address = secc.getAddress();
			second.get(monum);
			
		}
		
	}
	
}

}
