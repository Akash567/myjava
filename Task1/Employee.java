package Task1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Employe implements Comparable<Employe>{
	
	private int age;
	private String name;
	
	Employe(int age, String name){
		this.age = age;
		this.name = name;
	}

	@Override
	public int compareTo(Employe o) {
		if(this.age == o.age) {
			return 0;
		}
		else if(this.age < o.age) {
			return 1;
		}
		return -1;
	}

	@Override
	public String toString() {
		return "Employe [age=" + age + ", name=" + name + "]";
	}
}
public class Employee{
	public static void main(String []args) {
	
	Employe emp = new Employe(20,"Akash");
	Employe emp1 = new Employe(10,"Aravindh");
	Employe emp2 = new Employe(25,"Kiran");
	Employe emp3 = new Employe(15,"Yukesh");
	
	List<Employe> emplist = new ArrayList<Employe>();
	emplist.add(emp);
	emplist.add(emp1);
	emplist.add(emp2);
	emplist.add(emp3);
	Collections.sort(emplist);
	for(Employe empl : emplist) {
		System.out.println(empl);
	}
	
	}
}

