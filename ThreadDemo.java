
//public class ThreadDemo extends Thread {
//	public void run() {
//		try {
//			for (int i = 1; i <= 5; i++) {
//				System.out.println(i);
//				Thread.sleep(2000);
//			}
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//
//	public static void main(String[] args) {
//		ThreadDemo td = new ThreadDemo();
//		ThreadDemo td1 = new ThreadDemo();
//		ThreadDemo td2 = new ThreadDemo();
//		td.start();
//		td1.start();
//		td2.start();
//	}
//}

public class ThreadDemo implements Runnable {

	@Override
	public void run() {
		try {
			for (int i = 1; i <= 5; i++) {
				System.out.println(i);
				Thread.sleep(2000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		ThreadDemo td = new ThreadDemo();
		ThreadDemo td1 = new ThreadDemo();
		ThreadDemo td2 = new ThreadDemo();
		Thread t = new Thread(td);
		Thread t1 = new Thread(td1);
		Thread t2 = new Thread(td2);
		t.start();
		t1.start();
		t2.start();
//		t.run();
//		t1.run();
//		t2.run();
	}

}
